package cn.edu.hqu.cst.android.dyn.client;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

class ClientThread implements Runnable {
    public static Handler sendHandler;
    DatagramSocket remoteDs = null;
    InetAddress remoteAddr = null;
    private final static int remotePort = 5000;
    byte[] buf = new byte[1024];
    public ClientThread(){
        try{
            remoteDs = new DatagramSocket();
            remoteAddr = InetAddress.getByName("127.0.0.1");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void run(){
        new Thread(){
            //客户端子线程中创建子线程
            @Override
            public void run(){
                /**
                 * Looper对象通过MessageQueue来存放消息和事件。
                 * Handler对象和Looper进行交互，Handler可以看作Looper的一个接口，
                 * 用来向指定的Looper发送消息及定义处理方法。
                 * 在非主线程中直接new Handler()会报错，
                 * 因为非主线程中默认没有创建Looper对象，
                 * 需要先调用Looper.prepare()启用Looper。
                 */
                Looper.prepare();
                sendHandler = new Handler(){
                    @Override
                    public void handleMessage(Message msg){
                        if(msg.what == 0x1234){

                            buf = msg.getData().getString("sendContent").getBytes();
                            DatagramPacket remoteDp = new DatagramPacket(buf,buf.length,remoteAddr,remotePort);
                            try{
                                remoteDs.send(remoteDp);
                            }catch (IOException e){
                                e.printStackTrace();
                            }
                        }
                    }
            };
                //让Looper开始工作，从消息队列里获取消息，处理消息
                Looper.loop();
                /**
                 * Looper.loop()之后的代码不会被执行，这个函数内部应该是一个循环
                 * 当调用sendHandler.getLooper().quit()后loop才会终止，其后面的代码才能运行
                 */
        }
    }.start();


    }
}
