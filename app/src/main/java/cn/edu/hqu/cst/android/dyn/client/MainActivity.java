package cn.edu.hqu.cst.android.dyn.client;

import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {
    EditText et_main_content;
    Button btn_send;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        et_main_content = findViewById(R.id.et_main_content);
        btn_send = findViewById(R.id.btn_send);

        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                send();

            }
        });
        new Thread(new ClientThread()).start();


    }
    void send(){

        Message msg = new Message();
        msg.what = 0x1234;
        Bundle bundle = new Bundle();
        bundle.putString("sendContent", et_main_content.getText().toString());
        msg.setData(bundle);
        ClientThread.sendHandler.sendMessage(msg);
    }

}